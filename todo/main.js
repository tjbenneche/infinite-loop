var button = document.getElementById('new-todo')
var todoSection = document.getElementById('todos')

var todoList = []

var Todo = function(body) {
  this.complete = false
  this.text = body
  this.created = Date.now()
  todoNum++
}

function todoToDom(todo) {
  var domTodo = document.createElement('div')
  domTodo.dataset.complete = todo.complete
  domTodo.dataset.num = todo.num
  var title = document.createElement('span')
  title.innerHTML = todo.text
  domTodo.appendChild(title)
  todoSection.appendChild(domTodo)
}

button.addEventListener('click', function() {
  var body = prompt('What do you have to do?')
  var todo = new Todo(body)
  console.log(todo)
  todoList.push(todo)
  todoToDom(todo)
})
